package com.devcamp.bookapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookapi.model.Chapter;
import com.devcamp.bookapi.model.Lesson;
import com.devcamp.bookapi.repository.ChapterRepository;
import com.devcamp.bookapi.repository.LessonRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class LessonController {
    @Autowired
    ChapterRepository chapterRepository;
    @Autowired
    LessonRepository lessonRepository;

    @GetMapping("/lesson/all")
    public List<Lesson> getAllLessons() {
        return lessonRepository.findAll();
    }

    @PostMapping("/lesson/create/{id}")
    public ResponseEntity<Lesson> createLesson(@RequestBody Lesson pLesson, @PathVariable("id") long id) {
        Optional<Chapter> cChapter = chapterRepository.findById(id);
        if (cChapter.isPresent()) {
            try {
                Lesson nLesson = new Lesson();
                nLesson.setLeesonName(pLesson.getLeesonName());
                nLesson.setLessonCode(pLesson.getLessonCode());
                nLesson.setLessonIntroduce(pLesson.getLessonIntroduce());
                nLesson.setPage(pLesson.getPage());
                nLesson.setChapter(cChapter.get());

                return new ResponseEntity<>(lessonRepository.save(nLesson), HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/lesson/update/{id}")
    public ResponseEntity<Lesson> updateLesson(@PathVariable("id") long id,
            @RequestBody Lesson pLesson) {
        Optional<Lesson> cLesson = lessonRepository.findById(id);
        if (cLesson.isPresent()) {
            try {
                cLesson.get().setLessonCode(pLesson.getLessonCode());
                cLesson.get().setLeesonName(pLesson.getLeesonName());
                cLesson.get().setPage(pLesson.getPage());
                cLesson.get().setLessonIntroduce(pLesson.getLessonIntroduce());
                return new ResponseEntity<>(lessonRepository.save(cLesson.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/lesson/delete/{id}")
    public ResponseEntity<Lesson> deleteLesson(@PathVariable("id") long id) {
        lessonRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/lesson/details/{id}")
    public Lesson getLessonById(@PathVariable("id") long id) {
        return lessonRepository.findById(id).get();
    }

}
