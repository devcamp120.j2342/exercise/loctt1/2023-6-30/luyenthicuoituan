package com.devcamp.bookapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookapi.model.Chapter;
import com.devcamp.bookapi.repository.ChapterRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ChapterController {
    @Autowired
    ChapterRepository chapterRepository;

    @GetMapping("/chapter/all")
    public List<Chapter> getAllChapter() {
        return chapterRepository.findAll();
    }

    @PostMapping("/chapter/create")
    public ResponseEntity<Chapter> createChapter(@RequestBody Chapter pChapter) {
        try {
            return new ResponseEntity<Chapter>(chapterRepository.save(pChapter), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/chapter/update/{id}")
    public ResponseEntity<Chapter> updateChapter(@PathVariable("id") long id, @RequestBody Chapter pChapter) {
        Optional<Chapter> newChapter = chapterRepository.findById(id);
        if (newChapter.isPresent()) {
            try {

                newChapter.get().setChapterCode(pChapter.getChapterCode());
                newChapter.get().setChapterName(pChapter.getChapterName());
                newChapter.get().setChapterIntroduce(pChapter.getChapterIntroduce());
                newChapter.get().setChapterTranslator(pChapter.getChapterTranslator());
                newChapter.get().setChapterPage(pChapter.getChapterPage());
                newChapter.get().setLessons(pChapter.getLessons());
                return new ResponseEntity<Chapter>(chapterRepository.save(newChapter.get()), HttpStatus.OK);

            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/chapter/delete/{id}")
    public ResponseEntity<Chapter> deleteChapter(@PathVariable("id") long id) {
        chapterRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/chapter/details/{id}")
    public Chapter getChapterById(@PathVariable("id") long id) {
        return chapterRepository.findById(id).get();
    }
}
