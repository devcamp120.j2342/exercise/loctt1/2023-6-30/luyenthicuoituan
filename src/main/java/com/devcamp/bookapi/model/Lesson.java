package com.devcamp.bookapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lessons")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "lesson_code", unique = true)
    private String lessonCode;
    @Column(name = "lesson_name")
    private String lessonName;
    @Column(name = "lesson_introduce")
    private String lessonIntroduce;
    private int page;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chapter_id", nullable = false)
    private Chapter chapter;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLessonCode() {
        return lessonCode;
    }

    public void setLessonCode(String lessonCode) {
        this.lessonCode = lessonCode;
    }

    public String getLeesonName() {
        return lessonName;
    }

    public void setLeesonName(String leesonName) {
        this.lessonName = leesonName;
    }

    public String getLessonIntroduce() {
        return lessonIntroduce;
    }

    public void setLessonIntroduce(String lessonIntroduce) {
        this.lessonIntroduce = lessonIntroduce;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Lesson(long id, String lessonCode, String leesonName, String lessonIntroduce, int page, Chapter chapter) {
        this.id = id;
        this.lessonCode = lessonCode;
        this.lessonName = leesonName;
        this.lessonIntroduce = lessonIntroduce;
        this.page = page;
        this.chapter = chapter;
    }

    public Lesson() {
    }

    // public Chapter getChapter() {
    // return chapter;
    // }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

}
