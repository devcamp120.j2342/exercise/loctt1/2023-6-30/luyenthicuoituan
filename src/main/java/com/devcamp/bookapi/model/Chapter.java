package com.devcamp.bookapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "chapters")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "chapter_code", unique = true)
    private String chapterCode;
    @Column(name = "chapter_name")
    private String chapterName;
    @Column(name = "chapter_introduce")
    private String chapterIntroduce;
    @Column(name = "chapter_translator")
    private String chapterTranslator;
    @Column(name = "chapter_page")
    private int chapterPage;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "chapter")
    private Set<Lesson> lessons;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChapterCode() {
        return chapterCode;
    }

    public void setChapterCode(String chapterCode) {
        this.chapterCode = chapterCode;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterIntroduce() {
        return chapterIntroduce;
    }

    public void setChapterIntroduce(String chapterIntroduce) {
        this.chapterIntroduce = chapterIntroduce;
    }

    public String getChapterTranslator() {
        return chapterTranslator;
    }

    public void setChapterTranslator(String chapterTranslator) {
        this.chapterTranslator = chapterTranslator;
    }

    public int getChapterPage() {
        return chapterPage;
    }

    public void setChapterPage(int chapterPage) {
        this.chapterPage = chapterPage;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Chapter(long id, String chapterCode, String chapterName, String chapterIntroduce, String chapterTranslator,
            int chapterPage, Set<Lesson> lessons) {
        this.id = id;
        this.chapterCode = chapterCode;
        this.chapterName = chapterName;
        this.chapterIntroduce = chapterIntroduce;
        this.chapterTranslator = chapterTranslator;
        this.chapterPage = chapterPage;
        this.lessons = lessons;
    }

    public Chapter() {
    }

}
