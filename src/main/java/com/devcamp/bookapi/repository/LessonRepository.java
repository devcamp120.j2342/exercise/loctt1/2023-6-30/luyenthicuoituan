package com.devcamp.bookapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bookapi.model.Lesson;

public interface LessonRepository extends JpaRepository<Lesson, Long> {

}
