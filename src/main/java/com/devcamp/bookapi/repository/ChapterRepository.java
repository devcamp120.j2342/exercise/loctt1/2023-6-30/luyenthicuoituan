package com.devcamp.bookapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bookapi.model.Chapter;

public interface ChapterRepository extends JpaRepository<Chapter, Long> {

}
